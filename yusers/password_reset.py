"""Password reset views."""
import logging
from typing import Union
from urllib.parse import urljoin

from flask import Blueprint, redirect, request, url_for
from flask_babel import lazy_gettext as _
from flask_mail import Message
from flask_wtf import FlaskForm
from werkzeug.wrappers import Response
from wtforms import PasswordField, validators

from . import MAIL
from .ldap import search, set_password
from .tokens import PasswordResetTokenGenerator
from .utils import render_template

try:
    from wtforms import EmailField
except ImportError:
    from wtforms.fields.html5 import EmailField

_LOGGER = logging.getLogger(__name__)
PASSWORD_RESET_BLUEPRINT = Blueprint('password_reset', __name__, url_prefix='/password_reset')


class PasswordResetForm(FlaskForm):
    """Form to request a password reset."""
    email = EmailField('email', validators=[validators.InputRequired(), validators.Email()])


class PasswordSetForm(FlaskForm):
    """Form to set a new password."""
    new_password = PasswordField('new_password', validators=[validators.InputRequired()])
    new_password_2 = PasswordField('new_password_2',
                                   validators=[validators.EqualTo('new_password', _("Passwords don't match"))])


@PASSWORD_RESET_BLUEPRINT.route('/', methods=('GET', 'POST'))
def password_reset() -> Union[Response, str]:
    """Password reset form."""
    form = PasswordResetForm()
    if form.validate_on_submit():
        result = search('(&(objectclass=person)(maildrop={}))'.format(form.email.data),
                        ['uidNumber', 'userPassword', 'uid'])
        if result:
            uid_number = int(result[0][1]['uidNumber'][0])
            password = result[0][1]['userPassword'][0]
            username = result[0][1]['uid'][0].decode('utf-8')
            generator = PasswordResetTokenGenerator()
            path = url_for('password_reset.password_reset_confirm', uid_number=uid_number,
                           token=generator.make_token(uid_number, password))
            context = {
                'reset_url': urljoin(request.base_url, path),
                'username': username,
            }
            subject = render_template('password_reset/email_subject.txt')
            msg = Message(subject, recipients=[form.email.data])
            msg.body = render_template('password_reset/email.txt', **context)
            _LOGGER.info('Sending password reset mail to %s', form.email.data)
            MAIL.send(msg)
        else:
            _LOGGER.info('No account found for %s', form.email.data)
        return redirect(url_for('password_reset.password_reset_done'))
    context = {
        'form': form,
    }
    return render_template('password_reset/form.html', **context)


@PASSWORD_RESET_BLUEPRINT.route('/done/')
def password_reset_done() -> str:
    """Password reset done."""
    return render_template('password_reset/done.html')


@PASSWORD_RESET_BLUEPRINT.route('/confirm/<int:uid_number>/<token>/', methods=('GET', 'POST'))
def password_reset_confirm(uid_number: int, token: str) -> Union[Response, str]:
    """Confirm password reset and fill new password."""
    result = search('(&(objectclass=person)(uidNumber={}))'.format(uid_number), ['userPassword'])
    valid_link = False
    if result:
        password = result[0][1]['userPassword'][0]
        generator = PasswordResetTokenGenerator()
        if generator.check_token(uid_number, password, token):
            valid_link = True

    if not valid_link:
        _LOGGER.info('Invalid token %s or UID %s', token, uid_number)

    form = PasswordSetForm()
    if valid_link and form.validate_on_submit():
        set_password(result[0][0], form.new_password.data)
        return redirect(url_for('password_reset.password_reset_complete'))

    context = {
        'valid_link': valid_link,
        'form': form,
    }
    return render_template('password_reset/confirm.html', **context)


@PASSWORD_RESET_BLUEPRINT.route('/complete/')
def password_reset_complete() -> str:
    """Password reset complete."""
    return render_template('password_reset/complete.html')
