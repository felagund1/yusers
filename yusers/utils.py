"""Various utilities."""
import os

from flask import render_template as _render_template


def render_template(template_name: str, **context) -> str:
    """Render template with overrides."""
    return _render_template([os.path.join('overrides', template_name), template_name], **context)
