import os
from http import HTTPStatus
from unittest import TestCase

from jinja2 import ChoiceLoader, FileSystemLoader

from yusers import InstanceTemplateFlask

from .utils import FlaskTestCase


class InstanceTemplateFlaskTest(TestCase):
    def test_jinja_loader(self):
        app = InstanceTemplateFlask(__name__)
        app.config['YUSERS_CUSTOM_TEMPLATES_DIR'] = '/path/custom/templates'

        self.assertIsInstance(app.jinja_loader, ChoiceLoader)
        custom_loader, app_loader = app.jinja_loader.loaders
        self.assertIsInstance(custom_loader, FileSystemLoader)
        self.assertEqual(custom_loader.searchpath, ['/path/custom/templates'])
        self.assertIsInstance(app_loader, FileSystemLoader)
        self.assertEqual(app_loader.searchpath, [os.path.join(os.path.dirname(__file__), 'templates')])


class AppIndexTest(FlaskTestCase):
    def test_get(self):
        response = self.client.get('/')

        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        self.assertEqual(response.headers['Location'], '/password_reset/')
