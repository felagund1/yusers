from http import HTTPStatus
from typing import List, Optional
from unittest.mock import _Call, call, patch, sentinel

from freezegun import freeze_time
from ldap import SCOPE_SUBTREE

from yusers import MAIL
from yusers.ldap import USERS_BASE

from .utils import FlaskTestCase


class PasswordResetTest(FlaskTestCase):
    def setUp(self):
        super().setUp()
        patcher = patch('yusers.ldap.ReconnectLDAPObject', autospec=True)
        self.addCleanup(patcher.stop)
        self.ldap_mock = patcher.start()

    def assertLdapCalled(self) -> None:
        calls = [call('ldap://localhost:389/', retry_max=10, retry_delay=0.5),
                 call().simple_bind_s('uid=admin,ou=users,dc=yunohost,dc=org', 'Quagaars!'),
                 call().search_s(USERS_BASE, SCOPE_SUBTREE, '(&(objectclass=person)(maildrop=kryten@example.org))',
                                 ['uidNumber', 'userPassword', 'uid'])]
        self.assertEqual(self.ldap_mock.mock_calls, calls)

    def test_get(self):
        response = self.client.get('/password_reset/')

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertIn(b'Password reset', response.data)

    @freeze_time('2019-01-31')
    def test_post(self):
        email = 'kryten@example.org'
        self.ldap_mock.return_value.search_s.return_value = [
            (sentinel.uid, {'uidNumber': [b'42'], 'userPassword': [b'PASSWORD'], 'uid': [b'kryten']})]
        with MAIL.record_messages() as outbox:
            response = self.client.post('/password_reset/', data={'email': email})

        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        self.assertEqual(response.headers['Location'], '/password_reset/done/')
        # Check mail was sent.
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].recipients, [email])
        self.assertIn(' forgotten: kryten', outbox[0].body)
        self.assertIn('30-b667f57fcdde8848d41c5bd0333f51f69303289a', outbox[0].body)

        self.assertLdapCalled()

    @freeze_time('2019-01-31')
    def test_post_not_found(self):
        email = 'kryten@example.org'
        self.ldap_mock.return_value.search_s.return_value = []
        with MAIL.record_messages() as outbox:
            response = self.client.post('/password_reset/', data={'email': email})

        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        self.assertEqual(response.headers['Location'], '/password_reset/done/')
        # Check mail wasn't sent.
        self.assertEqual(len(outbox), 0)

        self.assertLdapCalled()

    def test_post_invalid(self):
        with MAIL.record_messages() as outbox:
            response = self.client.post('/password_reset/', data={'email': 'not-an-email'})

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertIn(b'Password reset', response.data)
        self.assertIn(b'Invalid email address.', response.data)
        # Check no mail was sent.
        self.assertEqual(len(outbox), 0)


class PasswordResetDoneTest(FlaskTestCase):
    def test_get(self):
        response = self.client.get('/password_reset/done/')

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertIn(b'Password reset sent', response.data)


class PasswordResetConfirmTest(FlaskTestCase):
    def setUp(self):
        super().setUp()
        patcher = patch('yusers.ldap.ReconnectLDAPObject', autospec=True)
        self.addCleanup(patcher.stop)
        self.ldap_mock = patcher.start()

    def assertLdapCalled(self, extra_calls: Optional[List[_Call]] = None) -> None:
        calls = [call('ldap://localhost:389/', retry_max=10, retry_delay=0.5),
                 call().simple_bind_s('uid=admin,ou=users,dc=yunohost,dc=org', 'Quagaars!'),
                 call().search_s(USERS_BASE, SCOPE_SUBTREE, '(&(objectclass=person)(uidNumber=42))',
                                 ['userPassword'])]
        self.assertEqual(self.ldap_mock.mock_calls, calls + (extra_calls or []))

    @freeze_time('2019-01-31')
    def test_get(self):
        self.ldap_mock.return_value.search_s.return_value = [(sentinel.uid, {'userPassword': [b'PASSWORD']})]

        response = self.client.get('/password_reset/confirm/42/30-b667f57fcdde8848d41c5bd0333f51f69303289a/')

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertIn(b'Enter new password', response.data)
        self.assertLdapCalled()

    def test_get_invalid(self):
        self.ldap_mock.return_value.search_s.return_value = [(sentinel.uid, {'userPassword': [b'PASSWORD']})]

        response = self.client.get('/password_reset/confirm/42/30-invalid/')

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertIn(b'Enter new password', response.data)
        self.assertIn(b'The password reset link was invalid.', response.data)
        self.assertLdapCalled()

    def test_get_not_found(self):
        self.ldap_mock.return_value.search_s.return_value = []

        response = self.client.get('/password_reset/confirm/42/30-token/')

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertIn(b'Enter new password', response.data)
        self.assertIn(b'The password reset link was invalid.', response.data)
        self.assertLdapCalled()

    @freeze_time('2019-01-31')
    def test_post(self):
        self.ldap_mock.return_value.search_s.return_value = [(sentinel.uid, {'userPassword': [b'PASSWORD']})]

        response = self.client.post('/password_reset/confirm/42/30-b667f57fcdde8848d41c5bd0333f51f69303289a/',
                                    data={'new_password': 'password', 'new_password_2': 'password'})

        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        self.assertEqual(response.headers['Location'], '/password_reset/complete/')
        # Check LDAP password changed.
        calls = [call('ldap://localhost:389/', retry_max=10, retry_delay=0.5),
                 call().simple_bind_s('uid=admin,ou=users,dc=yunohost,dc=org', 'Quagaars!'),
                 call().passwd_s(sentinel.uid, None, 'password')]
        self.assertLdapCalled(calls)

    @freeze_time('2019-01-31')
    def test_post_invalid(self):
        self.ldap_mock.return_value.search_s.return_value = [(sentinel.uid, {'userPassword': [b'PASSWORD']})]

        response = self.client.post('/password_reset/confirm/42/30-b667f57fcdde8848d41c5bd0333f51f69303289a/',
                                    data={'new_password': 'password', 'new_password_2': 'another'})

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertIn(b'Enter new password', response.data)
        self.assertIn(b'Passwords don&#39;t match', response.data)
        self.assertLdapCalled()

    def test_post_invalid_link(self):
        self.ldap_mock.return_value.search_s.return_value = [(sentinel.uid, {'userPassword': [b'PASSWORD']})]

        response = self.client.post('/password_reset/confirm/42/30-invalid/',
                                    data={'new_password': 'password', 'new_password_2': 'password'})

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertIn(b'Enter new password', response.data)
        self.assertIn(b'The password reset link was invalid.', response.data)
        self.assertLdapCalled()


class PasswordResetConfirmDoneTest(FlaskTestCase):
    def test_get(self):
        response = self.client.get('/password_reset/complete/')

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertIn(b'Password reset complete', response.data)
