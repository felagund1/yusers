from datetime import date

from freezegun import freeze_time

from yusers.tokens import PasswordResetTokenGenerator

from .utils import FlaskTestCase


class PasswordResetTokenGeneratorTest(FlaskTestCase):
    def test_num_days(self):
        gen = PasswordResetTokenGenerator()
        data = (
            (date(2019, 1, 1), 0),
            (date(2019, 1, 31), 30),
            (date(2019, 12, 31), 364),
            (date(2020, 1, 1), 365),
        )
        for today, num_days in data:
            with self.subTest(today=today):
                self.assertEqual(gen.num_days(today), num_days)

    @freeze_time('2019-01-31')
    def test_make_token(self):
        gen = PasswordResetTokenGenerator()
        with self.app.test_request_context():
            self.assertEqual(gen.make_token(42, 'PASSWORD'), '30-b667f57fcdde8848d41c5bd0333f51f69303289a')

    @freeze_time('2019-01-31')
    def test_check_token(self):
        gen = PasswordResetTokenGenerator()
        valid_tokens = ('27-3af333d571778e1355a2904ecc29e7878b555089',
                        '28-ed12bfa5eb8035f361bcd06badc14f030a0ec4f0',
                        '29-b9e21aa30d79e7cd467e514f90979880bc225619',
                        '30-b667f57fcdde8848d41c5bd0333f51f69303289a')
        with self.app.test_request_context():
            for token in valid_tokens:
                with self.subTest(token=token):
                    self.assertTrue(gen.check_token(42, 'PASSWORD', token))

    @freeze_time('2019-01-31')
    def test_check_token_invalid(self):
        gen = PasswordResetTokenGenerator()
        invalid_tokens = ('28-b9e21aa30d79e7cd467e514f90979880bc225619',  # date-token mismatch
                          '26-a0d6f35c184668c41982186d17ec59712869f9d0',  # stale
                          '',
                          '-',
                          'shit',
                          'bull-shit')
        with self.app.test_request_context():
            for token in invalid_tokens:
                with self.subTest(token=token):
                    self.assertFalse(gen.check_token(42, 'PASSWORD', token))
