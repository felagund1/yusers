"""Yusers - Yunohost application for user management."""
import os
from logging.config import dictConfig

import jinja2
from flask import Flask, redirect, url_for
from flask.helpers import locked_cached_property
from flask_babel import Babel
from flask_mail import Mail
from flask_wtf.csrf import CSRFProtect

__version__ = '0.5.3.post1'


BABEL = Babel()
CSRF = CSRFProtect()
MAIL = Mail()


def _redirect_index():
    return redirect(url_for('password_reset.password_reset'))


class InstanceTemplateFlask(Flask):
    """Flask object which searches templates in the instance directory."""

    @locked_cached_property
    def jinja_loader(self):
        """The Jinja loader for this package bound object."""
        return jinja2.ChoiceLoader([jinja2.FileSystemLoader(self.config['YUSERS_CUSTOM_TEMPLATES_DIR']),
                                    super().jinja_loader])


def create_app(**config):
    """Create and configure application."""
    app = InstanceTemplateFlask(__name__, instance_relative_config=True)
    app.config['YUSERS_CUSTOM_TEMPLATES_DIR'] = os.path.join(app.instance_path, 'templates')
    app.config['YUSERS_LDAP_URI'] = os.environ.get('YUSERS_LDAP_URI', 'ldap://localhost:389/')
    app.config['YUSERS_LDAP_ADMIN_DN'] = os.environ.get('YUSERS_LDAP_ADMIN_PASSWORD',
                                                        'uid=admin,ou=users,dc=yunohost,dc=org')
    app.config['YUSERS_LDAP_ADMIN_PASSWORD'] = os.environ.get('YUSERS_LDAP_ADMIN_PASSWORD')
    app.config['YUSERS_LOGGING'] = None
    app.config['YUSERS_PASSWORD_RESET_TIMEOUT'] = os.environ.get('YUSERS_PASSWORD_RESET_TIMEOUT', 3)
    app.config['BABEL_TRANSLATION_DIRECTORIES'] = 'locale'
    app.config.from_envvar('YUSERS_SETTINGS', silent=True)
    app.config.from_mapping(**config)

    # Set up logging
    if app.config['YUSERS_LOGGING'] is not None:
        dictConfig(app.config['YUSERS_LOGGING'])

    # Set up translations.
    BABEL.init_app(app)
    # Set up CSRF protection.
    CSRF.init_app(app)
    # Set up mail
    MAIL.init_app(app)

    # Ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from .password_reset import PASSWORD_RESET_BLUEPRINT
    app.register_blueprint(PASSWORD_RESET_BLUEPRINT)

    # Redirect index to password reset.
    app.add_url_rule('/', 'index', _redirect_index)

    return app
