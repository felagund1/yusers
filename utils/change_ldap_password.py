"""Simple script to test LDAP permissions for password change."""
from getpass import getpass

from ldap import SCOPE_SUBTREE
from ldap.ldapobject import LDAPObject, ReconnectLDAPObject

admin_dn = 'uid=admin,ou=users,dc=yunohost,dc=org'
admin_password = getpass("Admin password: ")
user_email = input("User email: ")
user_password = getpass("New password: ")

ldap_obj = ReconnectLDAPObject('ldap://localhost:389/', retry_max=10, retry_delay=0.5)
ldap_obj.simple_bind_s(admin_dn, admin_password)
print("Get data:")
result = ldap_obj.search_s('ou=users,dc=yunohost,dc=org', SCOPE_SUBTREE, '(&(objectclass=person)(maildrop={}))'.format(user_email), ['uidNumber', 'userPassword', 'uid'])
print(result)
username = result[0][1]['uid'][0].decode('utf-8')

print("Change password:")
ldap_obj.passwd_s('uid={},ou=users,dc=yunohost,dc=org'.format(username), None, user_password)
