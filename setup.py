from distutils.command.build import build

from setuptools import setup
from setuptools.command.sdist import sdist


class custom_build(build):

    sub_commands = [('compile_catalog', None)] + build.sub_commands


class custom_sdist(sdist):

    def run(self):
        self.run_command('compile_catalog')
        super().run()


def main():
    setup(author='Vlastimil Zíma',
          cmdclass={'build': custom_build, 'sdist': custom_sdist})


if __name__ == '__main__':
    main()
