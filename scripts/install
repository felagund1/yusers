#!/bin/bash

#=================================================
# GENERIC START
#=================================================
# IMPORT GENERIC HELPERS
#=================================================

source _common.sh
source /usr/share/yunohost/helpers

#=================================================
# MANAGE SCRIPT FAILURE
#=================================================
# Exit if an error occurs during the execution of the script
ynh_abort_if_errors

#=================================================
# RETRIEVE ARGUMENTS FROM THE MANIFEST
#=================================================

domain=$YNH_APP_ARG_DOMAIN
# Append / and the end.
path_url=$(python3 -c "path='$YNH_APP_ARG_PATH'; path = path if path.endswith('/') else path + '/'; print(path)")
is_public=$YNH_APP_ARG_IS_PUBLIC
language=$YNH_APP_ARG_LANGUAGE
admin_password=$YNH_APP_ARG_ADMIN_PASSWORD
default_sender=$YNH_APP_ARG_DEFAULT_SENDER

### If it's a multi-instance app, meaning it can be installed several times independently
### The id of the app as stated in the manifest is available as $YNH_APP_ID
### The instance number is available as $YNH_APP_INSTANCE_NUMBER (equals "1", "2"...)
### The app instance name is available as $YNH_APP_INSTANCE_NAME
###    - the first time the app is installed, YNH_APP_INSTANCE_NAME = ynhexample
###    - the second time the app is installed, YNH_APP_INSTANCE_NAME = ynhexample__2
###    - ynhexample__{N} for the subsequent installations, with N=3,4...
### The app instance name is probably what interests you most, since this is
### guaranteed to be unique. This is a good unique identifier to define installation path,
### db names...
app=$YNH_APP_INSTANCE_NAME

#=================================================
# CHECK IF THE APP CAN BE INSTALLED WITH THESE ARGS
#=================================================
### About --weight and --time
### ynh_script_progression will show to your final users the progression of each scripts.
### In order to do that, --weight will represent the relative time of execution compared to the other steps in the script.
### --time is a packager option, it will show you the execution time since the previous call.
### This option should be removed before releasing your app.
### Use the execution time, given by --time, to estimate the weight of a step.
### A common way to do it is to set a weight equal to the execution time in second +1.
### The execution time is given for the duration since the previous call. So the weight should be applied to this previous call.
ynh_script_progression --message="Validating installation parameters..." --time --weight=1

### If the app uses NGINX as web server (written in HTML/PHP in most cases), the final path should be "/var/www/$app".
### If the app provides an internal web server (or uses another application server such as uWSGI), the final path should be "/opt/yunohost/$app"
final_path=/var/www/$app
test ! -e "$final_path" || ynh_die --message="This path already contains a folder"

# Register (book) web path
ynh_webpath_register --app=$app --domain=$domain --path_url=$path_url

#=================================================
# STORE SETTINGS FROM MANIFEST
#=================================================
ynh_script_progression --message="Storing installation settings..." --time --weight=1

ynh_app_setting_set --app=$app --key=domain --value=$domain
ynh_app_setting_set --app=$app --key=path --value=$path_url
ynh_app_setting_set --app=$app --key=is_public --value=$is_public
ynh_app_setting_set --app=$app --key=language --value=$language
ynh_app_setting_set --app=$app --key=default_sender --value=$default_sender

ynh_app_setting_set --app=$app --key=final_path --value=$final_path

#=================================================
# STANDARD MODIFICATIONS
#=================================================
# INSTALL DEPENDENCIES
#=================================================
ynh_script_progression --message="Installing dependencies..." --time --weight=1

### `ynh_install_app_dependencies` allows you to add any "apt" dependencies to the package.
### Those deb packages will be installed as dependencies of this package.

ynh_install_app_dependencies $pkg_dependencies

#=================================================
# NGINX CONFIGURATION
#=================================================
ynh_script_progression --message="Configuring NGINX web server..." --time --weight=1

### `ynh_add_nginx_config` will use the file conf/nginx.conf

script_name=${path_url::-1}

# Create a dedicated NGINX config
ynh_add_nginx_config script_name

#=================================================
# CREATE DEDICATED USER
#=================================================
ynh_script_progression --message="Configuring system user..." --time --weight=1

# Create a system user
ynh_system_user_create --username=$app

# Add user to a www-data group. uWSGI requires that to change group of the uWSGI socket.
adduser yusers www-data

#=================================================
# SPECIFIC SETUP
#=================================================
# CREATE VIRTUALENV
#=================================================
ynh_script_progression --message="Setting up virtualenv..." --time --weight=1

virtualenv -p python3 $final_path/venv
$final_path/venv/bin/pip install $YNH_CWD/..

#=================================================
# SETUP SYSTEMD
#=================================================
ynh_script_progression --message="Configuring a systemd service..." --time --weight=1

### `ynh_systemd_config` is used to configure a systemd script for an app.
### It can be used for apps that use sysvinit (with adaptation) or systemd.
### Have a look at the app to be sure this app needs a systemd script.
### `ynh_systemd_config` will use the file conf/systemd.service

# Create a dedicated systemd config
ynh_add_systemd_config

#=================================================
# CONFIGURE YUSERS
#=================================================

cp $YNH_CWD/../conf/settings.py $final_path/settings.py

### `ynh_replace_string` is used to replace a string in a file.
### (It's compatible with sed regular expressions syntax)

ynh_replace_string --match_string="__SECRET_KEY__" --replace_string="$(ynh_string_random --length 64)" --target_file="$final_path/settings.py"
ynh_replace_string --match_string="__ADMIN_PASSWORD__" --replace_string="$admin_password" --target_file="$final_path/settings.py"
ynh_replace_string --match_string="__LANGUAGE__" --replace_string="$language" --target_file="$final_path/settings.py"
ynh_replace_string --match_string="__DEFAULT_SENDER__" --replace_string="$default_sender" --target_file="$final_path/settings.py"

#=================================================
# CONFIGURE UWSGI SOCKET
#=================================================
ynh_script_progression --message="Configuring uWSGI socket..." --time --weight=1

cp $YNH_CWD/../conf/tmpfiles.conf /etc/tmpfiles.d/$app.conf
ynh_replace_string --match_string="__APP__" --replace_string="$app" --target_file="/etc/tmpfiles.d/$app.conf"
# Run tmpfiles and create socket
systemd-tmpfiles --create "/etc/tmpfiles.d/$app.conf"

#=================================================
# CONFIGURE UWSGI
#=================================================

mkdir -p /etc/uwsgi/apps-available
mkdir -p /etc/uwsgi/apps-enabled

# Set up uwsgi.py
cp $YNH_CWD/../conf/uwsgi.py /etc/uwsgi/uwsgi_$app.py
ynh_replace_string --match_string="__APP__" --replace_string="$app" --target_file="/etc/uwsgi/uwsgi_$app.py"

# Set up uwsgi.ini
cp $YNH_CWD/../conf/uwsgi.ini /etc/uwsgi/apps-available/$app.ini
ynh_replace_string --match_string="__VIRTUALENV__" --replace_string="$final_path/venv" --target_file="/etc/uwsgi/apps-available/$app.ini"
ynh_replace_string --match_string="__APP__" --replace_string="$app" --target_file="/etc/uwsgi/apps-available/$app.ini"
ynh_replace_string --match_string="__FINAL_PATH__" --replace_string="$final_path" --target_file="/etc/uwsgi/apps-available/$app.ini"
ln -s /etc/uwsgi/apps-available/$app.ini /etc/uwsgi/apps-enabled/$app.ini

#=================================================
# STORE THE CONFIG FILE CHECKSUM
#=================================================

### `ynh_store_file_checksum` is used to store the checksum of a file.
### That way, during the upgrade script, by using `ynh_backup_if_checksum_is_different`,
### you can make a backup of this file before modifying it again if the admin had modified it.

# Calculate and store the config file checksum into the app settings
ynh_store_file_checksum --file="$final_path/settings.py"
ynh_store_file_checksum --file="/etc/uwsgi/uwsgi_$app.py"
ynh_store_file_checksum --file="/etc/uwsgi/apps-available/$app.ini"

#=================================================
# GENERIC FINALIZATION
#=================================================
# SECURE FILES AND DIRECTORIES
#=================================================

### For security reason, any app should set the permissions to root: before anything else.
### Then, if write authorization is needed, any access should be given only to directories
### that really need such authorization.

# Set permissions to app files
chown -R root: $final_path

#=================================================
# SETUP LOGROTATE
#=================================================
ynh_script_progression --message="Configuring log rotation..." --time --weight=1

### `ynh_use_logrotate` is used to configure a logrotate configuration for the logs of this app.
### Use this helper only if there is effectively a log file for this app.

# Use logrotate to manage application logfile(s)
ynh_use_logrotate --logfile="/var/log/$app/$app.log"

# Set owner and group of logs to yusers user
chown -R $app:$app /var/log/$app

#=================================================
# INTEGRATE SERVICE IN YUNOHOST
#=================================================
ynh_script_progression --message="Integrating service in YunoHost..." --time --weight=1

### `yunohost service add` integrates a service in YunoHost. It then gets 
### displayed in the admin interface and through the others `yunohost service` commands.
### (N.B.: this line only makes sense if the app adds a service to the system!)

yunohost service add $app --description="A short description of the app" --log="/var/log/$app/$app.log"

#=================================================
# START SYSTEMD SERVICE
#=================================================
ynh_script_progression --message="Starting a systemd service..." --time --weight=1

### `ynh_systemd_action` is used to start a systemd service for an app.
### Only needed if you have configure a systemd service

# Start a systemd service
ynh_systemd_action --service_name=$app --action="start" --log_path="/var/log/$app/$app.log"

#=================================================
# SETUP SSOWAT
#=================================================
ynh_script_progression --message="Configuring SSOwat..." --time --weight=1

# Make app public if necessary
if [ $is_public -eq 1 ]
then
    # Everyone can access the app.
    # The "main" permission is automatically created before the install script.
    ynh_permission_update --permission="main" --add="visitors"
fi

#=================================================
# RELOAD NGINX
#=================================================
ynh_script_progression --message="Reloading NGINX web server..." --time --weight=1

ynh_systemd_action --service_name=nginx --action=reload

#=================================================
# END OF SCRIPT
#=================================================

ynh_script_progression --message="Installation of $app completed" --time --last
